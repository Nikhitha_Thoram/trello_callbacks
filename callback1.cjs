const fs = require("fs");

const path = require("path");

const filePath = path.resolve(__dirname, "./boards.json");

const readJsonFile = (fileName) => {
  const jsonString = fs.readFileSync(fileName, "utf-8", (error, data) => {
    if (error) {
      console.error(error);
    }
  });
  return JSON.parse(jsonString);
};

const getBoardId = (boardId, callback) => {
  setTimeout(() => {
    if (boardId && typeof boardId === "string") {
      let boards;
      try {
        boards = readJsonFile(filePath);
        foundBoard = boards.find((board) => board.id === boardId);
        if (foundBoard) {
          callback(null, foundBoard);
        } else {
          callback(new Error("No board found for the given bordId"));
        }
      } catch (error) {
        callback(error, null);
      }
    } else {
      callback(new Error("Board id is not a string"));
    }
  }, 2 * 1000);
};

module.exports = getBoardId;
