const fs = require("fs");
const path = require("path");

const filePath = path.resolve(__dirname, "./lists_1.json");
const readJsonFile = (fileName) => {
  const jsonString = fs.readFileSync(fileName, "utf-8", (error, data) => {
    if (error) {
      console.error(error);
    }
  });
  return JSON.parse(jsonString);
};

const getBoardId = require("./callback1.cjs");
const getLists = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

const getAllDetailsOfListsAndCards = () => {
  setTimeout(() => {
    getBoardId("mcu453ed", (error, data) => {
      if (error) {
        console.error(error);
      } else {
        console.log(data);
      }
    });

    getLists("mcu453ed", (error, data) => {
      if (error) {
        console.error(error);
      } else {
        console.log(data);
      }
    });

    const lists = readJsonFile(filePath);
    const listKeys = Object.keys(lists);
    for (let key of listKeys) {
      const list = lists[key];
      for (let object of list) {
        getCards(object.id, (error, data) => {
          if (error) {
            console.error(error);
          } else {
            console.log(data);
          }
        });
      }
    }
  }, 2 * 1000);
};

module.exports = getAllDetailsOfListsAndCards;
