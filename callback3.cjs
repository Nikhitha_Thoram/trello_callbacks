const fs = require("fs");

const path = require("path");
const filePath = path.resolve(__dirname, "./cards.json");

const readJsonFile = (fileName) => {
  const jsonString = fs.readFileSync(fileName, "utf-8", (error, data) => {
    if (error) {
      console.error(error);
    }
  });
  return JSON.parse(jsonString);
};

const getCards = (baordId, callback) => {
  setTimeout(() => {
    if (baordId && typeof baordId === "string") {
      let lists = [];
      try {
        lists = readJsonFile(filePath);
        foundList = lists[baordId];
        if (foundList) {
          callback(null, foundList);
        } else {
          callback(new Error("No card found with given listId"));
        }
      } catch (error) {
        callback(error, null);
      }
    } else {
      callback(new Error("listId is not a string"));
    }
  });
};

module.exports = getCards;
