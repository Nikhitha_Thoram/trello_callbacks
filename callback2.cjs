const fs = require("fs");

const path = require("path");
const filePath = path.resolve(__dirname, "./lists_1.json");

const readJsonFile = (fileName) => {
  const jsonString = fs.readFileSync(fileName, "utf-8", (error, data) => {
    if (error) {
      console.error(error);
    }
  });
  return JSON.parse(jsonString);
};

const getLists = (baordId, callback) => {
  setTimeout(() => {
    if (baordId && typeof baordId === "string") {
      let lists = [];
      try {
        lists = readJsonFile(filePath);
        foundList = lists[baordId];
        if (foundList) {
          callback(null, foundList);
        } else {
          callback(new Error("No list found with given boardId"));
        }
      } catch (error) {
        callback(error, null);
      }
    } else {
      callback(new Error("baordId is not a string"));
    }
  });
};

module.exports = getLists;
