const getBoardId = require("./callback1.cjs");
const getLists = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

const getAllDetails = () => {
  setTimeout(() => {
    getBoardId("mcu453ed", (error, data) => {
      if (error) {
        console.error(error);
      } else {
        console.log(data);
      }
    });

    getLists("mcu453ed", (error, data) => {
      if (error) {
        console.error(error);
      } else {
        console.log(data);
      }
    });

    getCards("qwsa221", (error, data) => {
      if (error) {
        console.error(error);
      } else {
        console.log(data);
      }
    });
  }, 2 * 1000);
};

module.exports = getAllDetails;
